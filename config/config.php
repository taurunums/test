<?php
/**
 * PHP configuration file
 */

// Composer autoloader require
require_once __DIR__."./../vendor/autoload.php";

//Error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

// Logs
DEFINE('LOGS',realpath(__DIR__."/../var/log"));

// Checker if is docker enviroment, local enviroment, or live
if (MiscFacts::is_docker_env() || MiscFacts::is_local_env()){
    define('MYSQL_HOST', trim(getenv('MYSQL_SERVER')));
    define('MYSQL_USERNAME', trim(getenv('MYSQL_LOGIN')));
    define('MYSQL_PASSWORD', trim(getenv('MYSQL_LOGIN_PASS')));
    define('MYSQL_DATABASE', trim(getenv('MYSQL_DATABASE')));
}else{
    // Define your database
//    define('MYSQL_HOST', 'localhost');
//    define('MYSQL_USERNAME', 'test');
//    define('MYSQL_PASSWORD', 'root');
//    define('MYSQL_DATABASE', 'test');
}


/**
 * @param $key
 * @return array|bool|string
 * Post helper function, checks for post and avoids errors if post is not set. Checks if post is array or string, returns escaped value if post is set.
 */
function post($key){
    if (isset($_POST[$key])){
        if(is_array($_POST[$key])){
            return array_map( 'addslashes', $_POST[$key] );
        } else {
        return addslashes($_POST[$key]);
        }
    }else{
        return false;
    }
}







