<?php


class MiscFacts
{

    const SUCCESS = 'success';
    const INFO = 'info';
    const WARNING = 'warning';
    const DANGER = 'danger';

    /**
     * @return bool
     * Function for checking if current enviroment is docker
     */
    public static function is_docker_env()
    {
        if (trim(getenv('DOCKER_SERVER')) === 'local') {

            return true;
        }
        return false;
    }

    /**
     * @return bool
     * Function for checking if current enviroment is local
     */
    public static function is_local_env(){
        if (file_exists(__DIR__."/../../config/local.env")) {
            $dotenv = new Dotenv\Dotenv(__DIR__ . "/../../config/", 'local.env');
            $dotenv->load();
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $message
     * @param string $type
     * Add message helper, part of php validation
     */
    public static function add_message($message, $type = 'success'){
        if(!isset($_SESSION['message'])){
            $_SESSION['message'] = [];
        }
        $_SESSION['message'][] = ['text' => $message, 'type' => $type];
    }


    /**
     * @return array|mixed
     * Gets message in session, part of php validation
     */
    public static function get_message(){
        if(isset($_SESSION['message'])){
            $message = $_SESSION['message'];
            unset($_SESSION['message']);
            return $message;
        }else{
            return array();
        }
    }

    /**
     * Prints message stored in session
     */
    public static function print_message()
    {
        $messages = self::get_message();
        foreach ($messages as $msg) { ?>
            <div class="m-2 message">
                <div class="alert alert-<?= $msg['type'] ?> alert-dismissible fade show" role="alert">
                    <?= $msg['text'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?
        }
    }

    /**
     * @param $errors
     * Prints errors if there are any, part of php validation
     */
    public static function print_errors($errors)
    {
        if (is_string($errors)) $errors = array($errors);
        foreach ($errors as $error) {

            self::add_message($error, self::DANGER);
        }
    }


}