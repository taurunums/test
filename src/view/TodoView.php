<?php


class TodoView
{

    public function __construct()
    {

    }

    /**
     * Header function view, prints header html, includes css and jquery
     */
    function header(){
        ?>
            <!doctype html>
            <html lang="en">
            <head >
            <meta charset="UTF-8">
             <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                         <meta http-equiv="X-UA-Compatible" content="ie=edge">
             <title>Test Project</title>

                <!-- Css Includes -->
              <link rel="stylesheet" href="/assets/bower-asset/bootstrap/dist/css/bootstrap.min.css">
              <link rel="stylesheet" href="/assets/bower-asset/font-awesome/css/all.min.css">
              <link rel="stylesheet" href="/css/main.css">

              <!-- Jquery Include -->
              <script src="/assets/bower-asset/jquery/dist/jquery.min.js"></script>

              </head>
            <body class="bg-dark">
        <?
    }

    /**
     * Prints footer html, includes javascript
     */
    function footer(){
        ?>
            </body>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
                  <script src="/assets/bower-asset/jquery-ui/jquery-ui.min.js"></script>
                  <script type="text/javascript" src="/js/main.js"></script>
                  </html>
        <?
    }

    /**
     * Prints open container
     */
    function open_container(){
        ?>
            <div class="card row col-md-9 col-12 m-0 p-0 border mx-md-auto mx-0 my-5 rounded shadow-lg">
                <div class="card-header"><h3 class="py-3 w-100 m-0"></div>
            <div id="message-div"></div>
                <div class="card-body p-md-3 p-sm-1 p-0">

            <table class="rounded w-100 my-1 bg-white p-0 shadow" >
                <thead><tr><th class="py-2 text-center table-secondary">To do List - Drag & Drop for sorting</th></tr></thead>
                    <tbody id="todo-list">
        <?
    }

    /**
     * @param $id
     * @param $description
     * Prints to do list in table, id and description are passed in
     */
    function print_todo($id,$description){
        ?>
            <tr class='border py-2 m-0 text-left w-100 grab d-flex' data-id="<?=$id?>">
                <td class="border-right col-1 d-flex align-items-center justify-content-center id-td px-md-3 px-sm-1 p-0"><?=$id?></td>
                <td class="text-left col-10 d-flex align-items-center description-td px-md-3 px-sm-1 p-1"><?=$description?></td>
                <td class="col-1 border-left d-flex align-items-center justify-content-center check-td px-md-3 px-sm-1 p-0">
                    <input type="checkbox" class="checkBox" onclick="checkbox_change()" id="<?=$id?>" >
                </td>
            </tr>
        <?
    }

    /**
     * Close container and print form and button
     */
    function close_container(){
       ?>
            </tbody>
            </table>
            <div class="py-2 d-flex justify-content-end align-items-center">
                <button id="delete-all-button" type="submit" class="py-1 mr-2 btn btn-light btn-xs border-danger float-right" disabled><i class="fas fa-trash-alt">&nbsp;</i> Delete</button>
                <span class="border rounded px-2 py-1 border-dark">
                    <input type='checkbox' name="delete-all" id='delete-all' value='1' onclick="check_checkbox(this);">
                    <label for="delete-all" class="m-0">Select All</label>
                </span>
            </div>
                <form action="/" name="add" id="add-form" method="post" onclick="" class="shadow p-2 border my-3">
                    <textarea rows="3" type="text" name="description" onclick="" id="description" class="w-100 add-input p-2 my-2" placeholder="add new task"></textarea>

                    <a type="" class="btn btn-success text-white" id="add-button">+ NEW</a>
                </form>
                <a href="#" id="csv-link" class="d-inline-block">Export Table data into CSV</a>
                </div>
            </div>
        <?
    }

}