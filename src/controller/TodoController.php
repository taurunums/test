<?php


class TodoController{

    public $model,$view;

    /**
     * TodoController constructor.
     * Instancing Model and View
     */
    public function __construct()
    {
        $this->model = new TodoModel();
        $this->view = new TodoView();
    }

    /**
     * Index page function, just display html
     */
    public function index() {

        $this->view->header();
            $this->view->open_container();
                $this->printList();
            $this->view->close_container();
        $this->view->footer();
    }

    /**
     * Print list through foreach loop
     */
    public function printList(){
        $list = $this->model->get_todo();
        foreach($list as $item) {
            $this->view->print_todo($item['id'],$item['description']);
        }
    }

    /**
     * Add item function, checks for errors and adds item if everything is fine
     */
    public function ajaxAdd() {

        if(post('add')) {
            if ($this->model->add_todo(post('description'))) {
                $this->printList();
            } else {
                MiscFacts::print_errors($this->model->errors);
            }
            MiscFacts::print_message();
        }
    }

    /**
     * Delete items function, checks for errors and deletes items if everything is fine
     */
    public function ajaxDelete() {

        if(post('delete_all')){
            if($this->model->delete_all_todo(post('ids'))) {
                $this->printList();
            } else {
                MiscFacts::print_errors($this->model->errors);
            }
        }
    }

    /**
     * Sort items functions, checks for errors and updates list if everything is fine
     */
    public function ajaxSort() {

        if(post('sort')){
            if($this->model->update(post('ids'))) {
                $this->printList();
            } else {
                MiscFacts::print_errors($this->model->errors);
            }
        }
    }

}