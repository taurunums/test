<?php


class Database extends \mysqli
{

    /**
     * @var \mysqli
     */
    public static $connection;

    /**
     * @return \mysqli
     * Connection with database, if connected return connection
     */
    public static function connection(){
        if(self::$connection){
            return self::$connection;
        }else{
            self::$connection = mysqli_connect(MYSQL_HOST,MYSQL_USERNAME,MYSQL_PASSWORD,MYSQL_DATABASE)
            or die(self::$connection->connect_error);
            self::$connection->set_charset("utf8");
            return self::$connection;
        }
    }

}