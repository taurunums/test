<?php


class TodoModel extends BaseModel
{
    public $errors;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     * Gets all items in database and orders them by id
     */
    function get_todo() {
        $sql="select * from todo_list ORDER BY order_id";
        $result = $this->db->query($sql);
        return $result->fetch_all(1);
    }

    /**
     * @param $param
     * @return bool
     * Adds new items if there are no errors after validation
     */
    function add_todo($param) {
        $this->validation($param);
        if (empty($this->errors)) {
            $description = $this->db->real_escape_string($param);
            $sql = "select * from todo_list ";
            $res = $this->db->query($sql) or die($this->db->error);
            $sql="INSERT INTO todo_list set description='{$description}', order_id = {$res->num_rows}";
            $res = $this->db->query($sql) or die($this->db->error);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } else{
            return false;
        }
    }

    /**
     * @param $param
     * @return bool
     * Deletes selected items, array is passed in
     */
    function delete_all_todo($params) {
        foreach( $params as $param ) {
            $sanitized_array[] = intval( $param );
        }
        $ids = implode("','", $sanitized_array);
        $sql="delete from todo_list where id IN ('".$ids."')";
        $res = $this->db->query($sql) or die($this->db->error);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $ids_param
     * Updates list order
     */
    function update($ids_param) {
        foreach( $ids_param as $id ) {
            $sanitized_array[] = intval( $id );
        }
        foreach ($sanitized_array as $order => $id){
            $sql = "update todo_list set order_id='{$order}' where id='{$id}'";
            $res = $this->db->query($sql) or die($this->db->error);
        }
    }

    /**
     * @param $param
     * Validation function, checks if item already exists and if post is empty
     */
    public function validation($param)
    {
        if ($this->todo_validation($param)) {
            $this->errors[] = "You have same to-do item";
        }
        if(empty($_POST['description'])) {
            $this->errors[] = "Please enter text in the field.";
        }
    }

    /**
     * @param $param
     * @return bool
     * Validation helper function, checks in database and returns true if found
     */
    public function todo_validation($param)
    {
        $description=$this->db->real_escape_string($param);
        $sql = "SELECT * from todo_list where description ='{$description}'";
        $result = $this->db->query($sql) or die($this->db->error);
        $res = $result->fetch_assoc();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }


}