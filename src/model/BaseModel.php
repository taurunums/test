<?php


abstract class BaseModel
{

    /**
     * @var mysqli
     */
    public $db;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->init_db();


    }

    /**
     * Database initialization
     */
    function init_db()
    {
        $this->db = Database::connection();
    }
}