<?php

require_once __DIR__.'/../config/config.php';
require_once __DIR__.'/../src/router.php';


//Homepage
route('/', function () {
    $homepage=new TodoController();
    $homepage->index();
});

//Ajax add item page
route('/ajaxAdd', function () {
    $homepage=new TodoController();
    $homepage->ajaxAdd();
});

//Ajax delete item page
route('/ajaxDelete', function () {
    $homepage=new TodoController();
    $homepage->ajaxDelete();
});

//Ajax sort item page
route('/ajaxSort', function () {
    $homepage=new TodoController();
    $homepage->ajaxSort();
});

$action = $_SERVER['REQUEST_URI'];
dispatch($action);



