 $( document ).ready(function() {

     // jQuery for adding items, calls /ajaxAdd page
    $( "#add-button" ).click(function(  ) {
        var description=$('#description').val();
        $('#message-div').html('');
        $.ajax({
            type : 'post',
            url : '/ajaxAdd',
            data : {description : description, add: 'add'},
            success : function(data) {
                $('#description').val('');
                if($(data).hasClass('message')){
                    $('#message-div').append(data);
                }else{
                    $('#todo-list').html(data);
                }
            },
            error : function(data) {
                alert('Error while adding new task');
            }
        });
    });

    // jQuery for deleting items, calls /ajaxDelete page
     $("#delete-all-button").on('click',function () {
         var searchIDs = $("#todo-list input:checkbox:checked").map(function () {
             return $(this).attr('id');

         }).get();
         console.log(searchIDs);
         if($(searchIDs).length === 0){
             alert('Check out task to delete it');
         }
         else{
             var confirmation = confirm('Are you sure');
             if (confirmation) {
                 searchIDs.join(',');
                 $.ajax({
                     type: 'post',
                     url: '/ajaxDelete',
                     data: {ids: searchIDs, delete_all: 'delete_all'},
                     success: function (data) {
                         $('#todo-list').html(data);
                     },
                     error: function (data) {
                         alert('stefan delete eror');
                     }
                 });
             }
         }

     });

     // jQuery for sorting items, calls /ajaxSort page
     $( "#todo-list" ).sortable({
         cursor: 'move',
         opacity: 0.85,
         stop: function ( event, ui){
             var newIDs = $("tr").map(function () {
                 return $(this).attr('data-id');
             }).get();
             $.ajax({
                 type: 'post',
                 url: '/ajaxSort',
                 data: {ids: newIDs, sort: 'sort'},
                 success: function (data) {
                     // $('#todo-list').html(data);
                 },
                 error: function (data) {
                     alert('Sortable error');
                 }
             });
         }
     });

     // Csv link
     $("#csv-link").on('click', function (event) {

         exportTableToCSV.apply(this, [$('#todo-list'), 'export.csv']);
         // IF CSV, don't do event.preventDefault() or return false
         // We actually need this to be a typical hyperlink
     });
     checkbox_change();



 });


// Function for exporting table with items into CSV file
function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td),tr:has(th)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',
        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function (i, row) {
            var $row = $(row), $cols = $row.find('.description-td,th,.id-td');

            return $cols.map(function (j, col) {
                var $col = $(col), text = $col.text();

                return text.replace(/"/g, '""'); // escape double quotes

            }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"',

        // Data URI
        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    // console.log(csv);
    if (window.navigator.msSaveBlob) { // IE 10+
        //alert('IE' + csv);
        window.navigator.msSaveOrOpenBlob(new Blob([csv], {type: "text/plain;charset=utf-8;"}), "csvname.csv")
    }
    else {
        $(this).attr({ 'download': filename, 'href': csvData, 'target': '_blank' });
    }
}


 function checkbox_change(){
     var checkBoxes = $('#todo-list .checkBox');
     checkBoxes.change(function () {
         $('#delete-all-button').prop('disabled', checkBoxes.filter(':checked').length < 1);
         $('#delete-all').prop('checked', checkBoxes.filter(':not(:checked)').length < 1);
     });
     checkBoxes.change();

     $("#delete-all").change(function () {
         $("input:checkbox").prop('checked', $(this).prop("checked"));
     });


 }

function check_checkbox(obj){
    if ($(obj).is(":checked")) {
        $('#delete-all-button').attr('disabled',false)
    } else {
        $('#delete-all-button').attr('disabled','disabled');
    }
}



 $(document).bind("ajaxSend", function(){
     checkbox_change();
     $('#delete-all-button').prop('disabled',true);
     $("input:checkbox").prop('checked',false);
 });

















